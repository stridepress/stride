#from django.db import models
from django.template import loader
from django.http import HttpResponse

from wagtail.core.models import Page
from wagtail.core.fields import RichTextField
from wagtail.admin.edit_handlers import FieldPanel


class HomePage(Page):
    body = RichTextField(blank=True)

    content_panels = Page.content_panels + [
        FieldPanel('body', classname="full")
    ]

class StatPage(Page):
    def serve(self, request):
        with open('home/templates/home/stat_page.html') as f:
            stats = f.read()

        return HttpResponse(stats, content_type='text/html')

