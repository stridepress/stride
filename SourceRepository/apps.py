from django.apps import AppConfig


class SourcerepositoryConfig(AppConfig):
    name = 'SourceRepository'
