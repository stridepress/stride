from django import forms
from django.db import models

from modelcluster.fields import ParentalKey, ParentalManyToManyField
from modelcluster.models import ClusterableModel
from modelcluster.contrib.taggit import ClusterTaggableManager

from taggit.models import TaggedItemBase

from wagtail.search import index
from wagtail.core.models import Page
from wagtail.core.fields import RichTextField
from wagtail.snippets.models import register_snippet
from wagtail.images.edit_handlers import ImageChooserPanel
from wagtail.documents.models import Document
from wagtail.documents.edit_handlers import DocumentChooserPanel
from wagtail.admin.edit_handlers import FieldPanel, MultiFieldPanel


class RepoIndexPage(Page):
    def get_context(self, request):
        context = super().get_context(request)
        context["sources"] = Source.published_sources().order_by('-ret_date')
        context["page_title"] = "Source Repository"
        return context


class SourceHomePage(Page):
    body = RichTextField(blank=True)

    content_panels = Page.content_panels + [
        FieldPanel('body', classname="full")
    ]


class SourceTag(TaggedItemBase):
    content_object = ParentalKey('Source', related_name='tagged_items', on_delete=models.CASCADE)


class Source(ClusterableModel):
    title = models.CharField(max_length=255)
    orig_link = models.URLField("Original link")
    arch_link = models.URLField("Archived link")
    pub_date = models.DateField("Published date")
    ret_date = models.DateField("Retrieved date")
    content_file = models.ForeignKey(
        'wagtaildocs.Document',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+')
    comment = RichTextField(blank=True)
    public = models.BooleanField(default=False)
    tags = ClusterTaggableManager(through=SourceTag, blank=True)
    categories = ParentalManyToManyField('SourceCategory', blank=True)
    topic = models.ForeignKey('SourceTopic',  on_delete=models.SET_NULL, null=True, blank=True)
    publisher = models.ForeignKey('SourcePublisher', on_delete=models.PROTECT)

    panels = [
        MultiFieldPanel([
            FieldPanel('title'),
            FieldPanel('public')], heading='Source'),
        MultiFieldPanel([
        FieldPanel('orig_link'),
        FieldPanel('arch_link')], heading='Links'),
        MultiFieldPanel([
            FieldPanel('pub_date'),
            FieldPanel('ret_date')], heading='Dates'),
        DocumentChooserPanel('content_file'),
        MultiFieldPanel([
            FieldPanel('publisher'),
            FieldPanel('tags'),
            FieldPanel('categories', widget=forms.CheckboxSelectMultiple),
            FieldPanel('topic', widget=forms.RadioSelect),
            FieldPanel('comment')], heading='Stride Info')
    ]

    @staticmethod
    def published_sources():
        return Source.objects.filter(public=True)

    @staticmethod
    def sources_by_tag(tag):
        return Source.objects.filter(tags__name=tag)

    @staticmethod
    def sources_by_category(category):
        return Source.objects.filter(categories__name=category)

    @staticmethod
    def sources_by_topic(topic):
        return Source.objects.filter(topic__name=topic)

    @staticmethod
    def sources_by_publisher(publisher):
        return Source.objects.filter(publisher__name=publisher)


class SourceTagIndexPage(Page):
    def get_context(self, request):
        context = super().get_context(request)
        tag = request.GET.get('tag')

        if not tag:
            tagged = SourceTag.objects.all()
            tags = dict()
            for t in tagged:
                if not t.tag in tags:
                    tags[t.tag] = Source.objects.filter(tags__name=t.tag).count()

            context["tags"] = dict(sorted(tags.items(), key=lambda k: k[1], reverse=True))
            context["page_title"] = "All tags"
            context["tag_type"] = "tag"
            context["slug"] = "source_repo_tags"
        else:
            sources = Source.sources_by_tag(tag)
            context['sources'] = sources
            context["page_title"] = "Tag: {0}".format(tag)

        return context


class SourceCategoryIndexPage(Page):
    def get_context(self, request):
        context = super().get_context(request)
        category = request.GET.get('category')

        if not category:
            categorised = SourceCategory.objects.all()
            categories = dict()
            for c in categorised:
                if not c.name in categories:
                    categories[c.name] = Source.objects.filter(categories__name=c.name).count()

            context["tags"] = dict(sorted(categories.items(), key=lambda k: k[1], reverse=True))
            context["page_title"] = "All categories"
            context["tag_type"] = "category"
            context["slug"] = "source_repo_categories"
        else:
            sources = Source.sources_by_category(category)
            context['sources'] = sources
            context["page_title"] = "Category: {0}".format(category)

        return context


class SourceTopicIndexPage(Page):
    def get_context(self, request):
        context = super().get_context(request)
        topic = request.GET.get('topic')

        if not topic:
            marked = SourceTopic.objects.all()
            topics = dict()
            for t in marked:
                if not t.name in topics:
                    topics[t.name] = Source.objects.filter(topic__name=t.name).count()

            context["tags"] = dict(sorted(topics.items(), key=lambda k: k[1], reverse=True))
            context["page_title"] = "All topics"
            context["slug"] = "source_repo_topics"
            context["tag_type"] = "topic"
        else:
            sources = Source.sources_by_topic(topic)
            context['sources'] = sources
            context["page_title"] = "Topic: {0}".format(topic)

        return context


class SourcePublisherIndexPage(Page):
    def get_context(self, request):
        context = super().get_context(request)
        publisher = request.GET.get('publisher')

        if not publisher:
            marked = SourcePublisher.objects.all()
            publishers = dict()
            for p in marked:
                if not p.name in publishers:
                    publishers[p.name] = Source.objects.filter(publisher__name=p.name).count()

            context["tags"] = dict(sorted(publishers.items(), key=lambda k: k[1], reverse=True))
            context["page_title"] = "All publishers"
            context["slug"] = "source_repo_publishers"
            context["tag_type"] = "publisher"
        else:
            sources = Source.sources_by_publisher(publisher)
            context['sources'] = sources
            context["page_title"] = "Publisher: {0}".format(publisher)

        return context

@register_snippet
class SourceCategory(models.Model):
    name = models.CharField(max_length=255)
    icon = models.ForeignKey(
            'wagtailimages.Image',
            null=True,
            on_delete=models.SET_NULL, related_name='+')

    panels = [
        FieldPanel('name'),
        ImageChooserPanel('icon'),
    ]

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'source categories'

@register_snippet
class SourceTopic(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name

@register_snippet
class SourcePublisher(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name
