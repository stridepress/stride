from wagtail.contrib.modeladmin.options import ModelAdmin, modeladmin_register
from .models import Source


class SourceAdmin(ModelAdmin):
    model = Source
    menu_icon = 'link'
    menu_order = 200
    add_to_settings_menu = False
    exclude_from_explorer = False
    list_display = ('title',)
    list_filter = ('title', 'public')
    search_fields = ('title', 'comment')

# Now you just need to register your customised ModelAdmin class with Wagtail
modeladmin_register(SourceAdmin)
