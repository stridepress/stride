# Generated by Django 2.2.5 on 2019-09-29 10:18

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('SourceRepository', '0002_source_published'),
    ]

    operations = [
        migrations.RenameField(
            model_name='source',
            old_name='published',
            new_name='public',
        ),
    ]
